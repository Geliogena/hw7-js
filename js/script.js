/* Теоретичні питання.
1.Рядок можна створить, записавши символи різного типу даних в одинарні, подвійні лапки, або в шаблонні літерали (зворотні
  лапки). Ще один спосіб створення рядків в JavaScript, це використання конструктора String("створений рядок").
2. Різниці між використанням подвійних та одинарних лапок немає, але коли ми використовуємо символ апостроф "'" в рядку,
то краще використати подвійні лапки. В рядок у зворотніх лапках ми можемо додавати якісь значення за допомогою символів ${},
 наприклад змінну, при виводі це значення буде склеюватись з рядком і стане також рядком.
 3. Для порівняння рядків можна використовувати оператор строгого порівняння "===", але він не враховує регістру літер.
 Для такого порівняння треба літери рядків, які ми порівнюємо привести до маленького або великого регистру. Для цього є методи
 "toLowerCase", "toUpperCase".  Також можно порівнювати строки знаками "<" або ">", але це порівняння буде показувати наскількі
 перші літери рядка більші або меньші ,порівняння виконується літера за літерою, починаючи з першої. в залежності від присвоюємого
  номеру коду (Юнікоду) в ASCII системі. Краще за все скористуватися властивістю "length", якщо треба зробити порівняння по
  кількості символів в рядках.
4. При використанні метода Date.now() ми повертаємо кількість мілісекунд, минулих від 1 січня 1970 року.
5. Різниця в тому, що Date.now() повертає мілісекунди, а newDate() повертає об'єкт, в якому вказані рік, місяць, день, час.
 */
'use strict'

function isPalindrome(str){
  let strArray = [...str];
  let newArray = [];
  str = str.toLowerCase().replace(/\W/g, '');
  strArray.forEach(elem => {
    newArray.unshift(elem);
  });
  let reversedStr = newArray.join('');
  if(str === reversedStr){
    return true;
  } else {
    return false;
  }
}
console.log(isPalindrome('madam'));

function lengthCheck(str, maxlength){
  if (str.length <= maxlength){
    return true;
  } else if(str.length > maxlength){
    return false;
  }
}
console.log(lengthCheck('Hello happiness!', 16));

let birthday = prompt('Введіть дату народження в форматі: рік, місяць, число');
function get_current_age(date) {
let date1 = new Date();
let date2 = new Date(date);
  return Math.trunc((date1 - date2) / (24 * 3600 * 365.25 * 1000)) | 0;
}
console.log(get_current_age(birthday));



